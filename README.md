## Objective

This project is meant to provide a minimal example of a REST-like system with basic CRUD operations that can be used as 
reference.

Please note that this is not an application with real life logic. For exemple, the current implementation just adds a new
author for each book. In real life you would like to make a validation and, if already exist, reuse the author.

The code will be commented with links to external tutorials/documentation/sources.

Any improvement, both on code and explanation, will be well-received.

## What I use

* Spring Boot to provide out-of-the box configurations
* Spring Data JPA to database interaction (database operations and object to database mapping)
* H2 database to make the project self-sufficient
* Spring Web to expose endpoints
* Lombok to reduce code verbosity
* Jackson (declared implicitly by Spring Web) to JSON generation

> Notice that Lombok might require additional IDE configuration. Please refer to the [oficial page](https://projectlombok.org/setup/overview).