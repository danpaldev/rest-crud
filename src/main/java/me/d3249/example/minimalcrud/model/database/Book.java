package me.d3249.example.minimalcrud.model.database;

import lombok.*;

import javax.persistence.*;

@Data //Lombok annotation to generate hashCode, equals, getters and setters https://projectlombok.org/features/Data
@NoArgsConstructor(access = AccessLevel.PROTECTED, force = true)
@AllArgsConstructor //Lombok annotations https://projectlombok.org/features/constructor
@Entity //JPA annotation https://www.baeldung.com/jpa-entities
public class Book {

    @Id
    private String ISBN;
    private String title;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn
    private Author author;
}
