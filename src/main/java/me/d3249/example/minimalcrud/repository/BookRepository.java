package me.d3249.example.minimalcrud.repository;

import me.d3249.example.minimalcrud.model.database.Book;
import org.springframework.data.repository.PagingAndSortingRepository;

//https://www.baeldung.com/spring-data-repositories
public interface BookRepository extends PagingAndSortingRepository<Book, String> {

}
