package me.d3249.example.minimalcrud.controller;

import lombok.RequiredArgsConstructor;
import me.d3249.example.minimalcrud.model.database.Author;
import me.d3249.example.minimalcrud.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController //https://www.baeldung.com/rest-with-spring-series
@RequestMapping("/author")
@RequiredArgsConstructor(onConstructor_ = {@Autowired}) //Lombok annotation + Spring annotation
//https://projectlombok.org/features/constructor
// https://www.baeldung.com/spring-autowire
public class AuthorController {

    private final AuthorService service;

    @GetMapping("")
    public Iterable<Author> searchByNames(@RequestParam(value = "fn", defaultValue = "") String firstName,
                                          @RequestParam(value = "ln", defaultValue = "") String lastName) {

        return service.authorsByNameLike(firstName, lastName);
    }

    @GetMapping("/book")
    public Author byBootTitle(@RequestParam(value = "title") String bookTitle) {
        return service.authorOfBook(bookTitle);
    }
}
